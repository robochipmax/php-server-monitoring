<?php

define('PSM_DB_PREFIX', 'monitor_');
define('PSM_DB_USER', 'root');
define('PSM_DB_PASS', '');
define('PSM_DB_NAME', 'dev_phpmon');
define('PSM_DB_HOST', 'localhost');
define('PSM_DB_PORT', '3306');
define('PSM_BASE_URL', '/phpmon');
