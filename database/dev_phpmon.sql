/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : dev_phpmon

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2019-01-30 15:23:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for monitor_config
-- ----------------------------
DROP TABLE IF EXISTS `monitor_config`;
CREATE TABLE `monitor_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_config
-- ----------------------------
INSERT INTO `monitor_config` VALUES ('language', 'en_US');
INSERT INTO `monitor_config` VALUES ('proxy', '0');
INSERT INTO `monitor_config` VALUES ('proxy_url', '');
INSERT INTO `monitor_config` VALUES ('proxy_user', 'admin@admin.com');
INSERT INTO `monitor_config` VALUES ('proxy_password', 'qwerty1386');
INSERT INTO `monitor_config` VALUES ('email_status', '0');
INSERT INTO `monitor_config` VALUES ('email_from_email', 'adi8hamzah@gmail.com');
INSERT INTO `monitor_config` VALUES ('email_from_name', 'Server Monitor');
INSERT INTO `monitor_config` VALUES ('email_smtp', '0');
INSERT INTO `monitor_config` VALUES ('email_smtp_host', '');
INSERT INTO `monitor_config` VALUES ('email_smtp_port', '');
INSERT INTO `monitor_config` VALUES ('email_smtp_security', '');
INSERT INTO `monitor_config` VALUES ('email_smtp_username', '');
INSERT INTO `monitor_config` VALUES ('email_smtp_password', '');
INSERT INTO `monitor_config` VALUES ('sms_status', '0');
INSERT INTO `monitor_config` VALUES ('sms_gateway', 'mollie');
INSERT INTO `monitor_config` VALUES ('sms_gateway_username', 'username');
INSERT INTO `monitor_config` VALUES ('sms_gateway_password', 'password');
INSERT INTO `monitor_config` VALUES ('sms_from', '1234567890');
INSERT INTO `monitor_config` VALUES ('pushover_status', '0');
INSERT INTO `monitor_config` VALUES ('pushover_api_token', '');
INSERT INTO `monitor_config` VALUES ('password_encrypt_key', '448c3d103b25212a0eecdf2792016d013ee263a8');
INSERT INTO `monitor_config` VALUES ('alert_type', 'status');
INSERT INTO `monitor_config` VALUES ('log_status', '1');
INSERT INTO `monitor_config` VALUES ('log_email', '0');
INSERT INTO `monitor_config` VALUES ('log_sms', '1');
INSERT INTO `monitor_config` VALUES ('log_pushover', '1');
INSERT INTO `monitor_config` VALUES ('log_retention_period', '365');
INSERT INTO `monitor_config` VALUES ('version', '3.2.0');
INSERT INTO `monitor_config` VALUES ('version_update_check', '3.2.0');
INSERT INTO `monitor_config` VALUES ('auto_refresh_servers', '1000');
INSERT INTO `monitor_config` VALUES ('show_update', '0');
INSERT INTO `monitor_config` VALUES ('last_update_check', '1541908883');
INSERT INTO `monitor_config` VALUES ('cron_running', '1');
INSERT INTO `monitor_config` VALUES ('cron_running_time', '0');

-- ----------------------------
-- Table structure for monitor_log
-- ----------------------------
DROP TABLE IF EXISTS `monitor_log`;
CREATE TABLE `monitor_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) unsigned NOT NULL,
  `type` enum('status','email','sms','pushover') NOT NULL,
  `message` varchar(255) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_log
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_log_users
-- ----------------------------
DROP TABLE IF EXISTS `monitor_log_users`;
CREATE TABLE `monitor_log_users` (
  `log_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`log_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_log_users
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_servers
-- ----------------------------
DROP TABLE IF EXISTS `monitor_servers`;
CREATE TABLE `monitor_servers` (
  `server_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `skpd_name` varchar(255) DEFAULT NULL,
  `descriptions` text,
  `source_code` varchar(255) DEFAULT '1',
  `server_owned` varchar(255) DEFAULT '1',
  `ip` varchar(500) NOT NULL,
  `port` int(5) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` enum('ping','service','website') NOT NULL DEFAULT 'website',
  `pattern` varchar(255) DEFAULT NULL,
  `status` enum('on','off') NOT NULL DEFAULT 'on',
  `error` varchar(255) DEFAULT NULL,
  `rtime` float(9,7) DEFAULT NULL,
  `last_online` datetime DEFAULT NULL,
  `last_check` datetime DEFAULT NULL,
  `active` enum('yes','no') NOT NULL DEFAULT 'yes',
  `email` enum('yes','no') NOT NULL DEFAULT 'no',
  `sms` enum('yes','no') NOT NULL DEFAULT 'no',
  `pushover` enum('yes','no') NOT NULL DEFAULT 'no',
  `warning_threshold` mediumint(1) unsigned NOT NULL DEFAULT '1',
  `warning_threshold_counter` mediumint(1) unsigned NOT NULL DEFAULT '0',
  `timeout` smallint(1) unsigned DEFAULT NULL,
  `website_username` varchar(255) DEFAULT NULL,
  `website_password` varchar(255) DEFAULT NULL,
  `stats` text,
  PRIMARY KEY (`server_id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_servers
-- ----------------------------
INSERT INTO `monitor_servers` VALUES ('1', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Sistem Informasi dan Manajemen Kepegawaian', 'Ada', 'Milik Sendiri', 'simpeg.bandung.go.id/', '80', 'SIMPEG', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('2', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Remunerasi Kinerja berbasis elektronik', 'Ada', 'Milik Sendiri', 'kinerja.bandung.go.id', '80', 'eRK', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('3', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Sistem Informasi Administrasi Presensi', 'Ada', 'Milik Sendiri', 'siap.bandung.go.id', '80', 'SIAP', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('4', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Sistem informasi e Formasi', 'Ada', 'Milik Sendiri', 'sifor.bandung.go.id', '80', 'SIFOR', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('5', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Sistem informasi  manajemen kearsipan pegawai', 'Ada', 'Milik Sendiri', 'simarsip.bandung.go.id', '80', 'SIM Arsip', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('6', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Sistem Informasi Manajemen Pengembangan Karir', '', 'Milik Sendiri', 'simekar.bandung.go.id', '80', 'SIMEKAR', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('7', 'Badan Pengelolaan Keuangan dan Aset', 'Website Untuk Pendaftaran Hibah Bansos Online', 'Ada', 'Milik Sendiri', 'sabiulungan.bandung.go.id', '80', 'Website Sabilulungan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('8', 'Badan Pengelolaan Pendapatan Daerah', 'Modul Database Wajib Pajak Self Assessment di Kota Bandung', 'Ada', 'Milik Sendiri', '172.16.20.1/mpd', '80', 'MPD (Modul Pajak Daerah)', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('9', 'Badan Pengelolaan Pendapatan Daerah', '-', 'Ada', 'Milik Sendiri', 'sipp.bppd.bandung.go.id', '80', 'SIPP', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('10', 'Badan Pengelolaan Pendapatan Daerah', '-', 'Ada', 'Milik Sendiri', 'esatria.bppd.bandung.go.id', '80', 'e-Satria', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('11', 'Badan Pengelolaan Pendapatan Daerah', '-', 'Ada', 'Milik Sendiri', 'sipanda.bppd.bandung.go.id', '80', 'Sistem Informasi Retribusi', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('12', 'Badan Pengelolaan Pendapatan Daerah', '-', 'Ada', 'Milik Sendiri', 'tapping.bppd.bandung.go.id', '80', 'Tapping Online', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('13', 'Badan Pengelolaan Pendapatan Daerah', '-', 'Ada', 'Milik Sendiri', 'bppd.bandung.go.id', '80', 'Website BPPD', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('14', 'Badan Perencanaan Pembangunan, Penelitian dan Pengembangan', 'Aplikasi sistem informasi yang menghimpun, mengelola dan menosialisasikan produk litbang secara terintegrasi', 'Ada', 'Milik Sendiri', 'litbangkotabandung.com/v2', '80', 'E-Litbang', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('15', 'Bagian Hubungan Masyarakat', 'Aplikasi untuk memonitoring berita baik cetak maupun online', 'Tidak Ada', 'Milik Sendiri', 'imm.today', '80', 'Monitoring Media Online', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('16', 'Bagian Hubungan Masyarakat', 'Aplikasi sebagai pengarsipan untuk kliping berita seputar pemerintah kota bandung', 'Tidak Ada', 'Milik Sendiri', 'kliping.bandung.go.id', '80', 'Arsip Kliping', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('17', 'Bagian Hubungan Masyarakat', 'Informasi kegiatan pimpinan dan Kepala OPD Kota Bandung', '', '', 'Humas.bandung.go.id', '80', 'Portal Berita', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('18', 'Bagian Hubungan Masyarakat', 'Informasi perizinan penawaran iklan, release berita dan dokumentasi, serta disposisi sambutan', '', '', 'internal.humas.bandung.go.id', '80', 'Internal', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('19', 'Bagian Hukum', 'Dalam rangka meningkatkan pemahaman dan pengetahuan mengenai hukum dalam menunjang penyelenggaraan pemerintahan di Kota Bandung maka diperlukan suatu informasi hukum yang tertata dan terselenggara dengan baik dalam suatu sistem. Hal tersebut diperlukan gu璳ఀ꟠璳\0ꟻ璳\0Ǖ\0\0磰ఀ௧疪潄瓅磰ఀ\0\0\0磰ఀ\0\0Ā\0Ⳗ疪磰ఀ`\0疩Ѐ\0\0\0\0೸崿沓梢೹ᓧ', 'Ada', 'Milik Sendiri', 'http://jdih.bandung.go.id', '80', 'JDIH KOTA BANDUNG', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('20', 'Bagian Kerjasama', 'Website Bagian Kerjasama Setda Kota Bandung', 'Ada', 'Milik Sendiri', 'http://kerjasama.bandung.go.id', '80', 'Website Bagian Kerjasama Setda Kota Bandung', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('21', 'Bagian Kerjasama', '', '', '', 'eseksama.bandung.go.id', '80', 'e saksama', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('22', 'Bagian Kesra dan Kemasyarakatan', 'Aplikasi untuk mendata para penyandang disabilitas di Kota Bandung', 'Ada', 'Milik Sendiri', 'pedikesra.bandung.go.id', '80', 'Data Penyandang Disabilitas', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('23', 'Bagian Layanan Pengadaan', 'Aplikasi proses pengadaan barang/jasa', 'Tidak Ada', 'Milik Sendiri', 'http://lpse.bandung.go.id', '80', 'Layanan Pengadaan secara Elektronik (LPSE)', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('24', 'Bagian Organisasi dan Pemberdayaan Aparatur Daerah', 'Sistem Informasi Akuntabilitas Kinerja Instansi Pemerintah', 'Ada', 'Milik Sendiri', 'lakip.bandung.go.id', '80', 'SILAKIP', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('25', 'Bagian Pemerintahan', 'Sistem Informasi Penilaian Bandung Juara', 'Ada', 'Milik Sendiri', 'http://sip.bandung.go.id', '80', 'SIP Bdg Juara', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('26', 'Bagian Perekonomian', 'Berisi realisasi dan tata cara pelaksanaan CSR di Kota Bandung', 'Ada', 'Milik Sendiri', 'tjssl.bandung.go.id', '80', 'Web TJSL', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('27', 'Bagian Program, Desain dan Kualitas Pembangunan', 'Aplikasi TEPRA adalah aplikasi yang dapat menjadi alat guna mempercepat penyerapan anggaran, dan memastikan APBN/APBD tepat sasaran sesuai dengan perencanaan yang tertuang dalam dokumen perencanaan pembangunan nasional. Selain itu, TEPRA juga diharapkan d璳ఀ꟠璳\0ꟻ璳\0Ǖ\0\0磰ఀ௧疪潄瓅磰ఀ\0\0\0磰ఀ\0\0Ā\0Ⳗ疪磰ఀ`\0疩Ѐ\0\0\0\0೸崿沓梢೹ᓧ沓೸嗥沘廄沓冊೹梮೹崿沓梢೹ᓧ沓梮೹ꛜ沔梢೹ꜰ沔\0\0䭢೹嶪沓̈沚ᕄ沓梮೹̈沚秐沓䭢೹秚沓', 'Tidak Ada', 'Milik Sendiri', 'monev.lkpp.go.id/tepra', '80', 'Sistem Monitoring Tim Evaluasi dan Pengawasan Realisasi Anggaran (SismonTEPRA)', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('28', 'Bagian Program, Desain dan Kualitas Pembangunan', '-', 'Ada', 'Milik Sendiri', 'simdp.bandung.go.id', '80', 'Sistem Informasi Desain Pembangunan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('29', 'Dinas Kependudukan dan Pencatatan Sipil', 'Selesai Dalam Genggaman', 'Ada', 'Milik Sendiri', 'disdukcapil.bandung.go.id', '80', 'Salaman', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('30', 'Dinas Kesehatan', 'Sistem Informasi Kesehatan Daerah', 'Ada', 'Milik Sendiri', '124.81.106.199', '80', 'SIKDA', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('31', 'Dinas Kesehatan', 'website dinas kesehatan kota bandung', 'Ada', 'Milik Sendiri', 'dinkes.bandung.go.id', '80', 'Website Dinas Kesehatan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('32', 'Dinas Kesehatan', 'Decision Support System', 'Ada', 'Milik Sendiri', '124.81.106.201', '80', 'DSS', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('33', 'Dinas Kesehatan', 'Data Wharehouse', 'Ada', 'Milik Sendiri', '124.81.106.198', '80', 'DW', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('34', 'Dinas Kesehatan', 'Storege Data Dinas Kesehatan', 'Ada', 'Milik Sendiri', 'clouddinkes.bandung.go.id', '80', 'Clouddinkes', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('35', 'Dinas Komunikasi dan Informatika', 'Merupakan Aplikasi Pengaduan melalui website, SMS, mobile, twitter', 'Tidak Ada', 'Milik Sendiri', 'https://www.lapor.go.id/', '80', 'Aplikasi LAPOR!', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('36', 'Dinas Komunikasi dan Informatika', '-', 'Ada', 'Milik Sendiri', 'http://simpeg.bandung.go.id/', '80', 'Simpeg Administratif', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('37', 'Dinas Komunikasi dan Informatika', 'aplikasi untuk menilai kematangan Smart City', 'Ada', 'Milik Sendiri', 'smartcity.bandung.go.id', '80', 'Forum Bandung smart City', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('38', 'Dinas Komunikasi dan Informatika', 'Portal Informasi Seputar Pemerintahan Kota Bandung', 'Ada', 'Milik Sendiri', 'bandung.go.id', '80', 'Website Portal Bandung', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('39', 'Dinas Komunikasi dan Informatika', '-', 'Ada', 'Milik Sendiri', 'https://ppid.bandung.go.id', '80', 'Website PPID', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('40', 'Dinas Komunikasi dan Informatika', 'Pertukaran Informasi Data Sehingga Data Dapat Langsung Diambil Melalui Aplikasi Ini', 'Ada', 'Milik Sendiri', 'http://mantra.bandung.go.id/', '80', 'Aplikasi Manajemen Integrasi dan Pertukaran Data (MANTRA)', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('41', 'Dinas Komunikasi dan Informatika', 'Aplikasi Hibah Non APBD ini merupakan aplikasi berbasis web. Aplikasi ini mengelola Hibah non APBD, pemberian dari perusahaan ke pemerintah', 'Ada', 'Milik Sendiri', 'hibah.bandung.go.id', '80', 'Hibah Non APBD', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('42', 'Dinas Koperasi, Usaha Mikro, Kecil dan Menengah', 'Website DInas', 'Ada', 'Milik Sendiri', 'diskopumkm.bandung.go.id', '80', 'Website Dinas Koperasi UMKM', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('43', 'Dinas Lingkungan Hidup dan Kebersihan', 'Sistem informasi untuk pendaftaran dokumen lingkungan', 'Ada', 'Milik Sendiri', 'dlhk.bandung.go.id/simdokling', '80', 'SIMDOKLING (dokumen lingkungan)', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('44', 'Dinas Lingkungan Hidup dan Kebersihan', 'Sistem informasi menampilkan hasil pengukuran kualitas udara', 'Ada', 'Milik Sendiri', 'aqms.bandung.go.id', '80', 'Dashboard Kualitas Udara', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('45', 'Dinas Pangan dan Pertanian', 'Aplikasi Informasi Pangan dan Pertanian Kota Bandung', 'Ada', 'Milik Sendiri', 'http://dispangtan-kotabandung.com/e-program/', '80', 'e- Program', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('46', 'Dinas Pemuda dan Olahraga', 'Untuk mempermudah menyewa SOR dan GOR yang dikelola Pemerintah Kota Bandung', 'Ada', 'Milik Sendiri', 'http://dispora.bandung.go.id/disporaPortal/', '80', 'Layanan & Pengelola SOR, GOR, GT', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('47', 'Dinas Pemuda dan Olahraga', 'Meningkatkan transparansi untuk melihat pendapatan secara Realtime', 'Ada', 'Milik Sendiri', 'http://dispora.bandung.go.id/disporaVenue/', '80', 'Penerimaan Restribusi', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('48', 'Dinas Pemuda dan Olahraga', 'Monitoring Pendapatan', 'Ada', 'Milik Sendiri', 'http://dispora.bandung.go.id/disporaLaporan/', '80', 'Evaluasi dan Pelaporan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('49', 'Dinas Pemuda dan Olahraga', 'Aplikasi untuk pendaftaran wirausaha baru', 'Ada', 'Milik Sendiri', 'dispora.bandung.go.id/aksiprabu', '80', 'Sistem Informasi Wirausaha Baru(Aksi Prabu)', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('50', 'Dinas Pemuda dan Olahraga', 'Sistem Informasi Manajemen Persuratan dan Disposisi', 'Ada', 'Milik Sendiri', 'dispora.bandung.go.id/simansurdisisi', '80', 'Sistem Informasi Manajemen Surat dan Disposisi Simansurdisisi', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('51', 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu', 'Bandung One Stop Service (konsep pengembangan bentuk layanan sebagai mana diatur Permendagri Nomor 24 Tahun 2006)', 'Ada', 'Milik Sendiri', 'www.boss.or.id', '80', 'BOSS', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('52', 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu', '-', 'Ada', 'Milik Sendiri', 'dpmptsp.bandung.go.id', '80', 'Hayu Bandung', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('53', 'Dinas Pendidikan', 'Aplikasi untuk mengolah Data Pokok Pendidikan Kota Bandung', 'Ada', 'Milik Sendiri', 'simdik.banndung.go.,id', '80', 'Data Dapodik Bandung', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('54', 'Dinas Pengendalian Penduduk dan Keluarga Berencana', 'Aplikasi berbasis web, yang merupakan otomatisasi pencatatan data keluarga yang terdiri dari data : demografi, KB, dan data kesejahteraan keluarga', 'Ada', 'Milik Sendiri', 'bppkbkotabandung.net/sitikencana', '80', 'SITIKENCANA', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('55', 'Dinas Perdagangan dan Perindustrian', 'Aplikasi untuk pendaftaran Surat Keterangan Asal (SKA) bagi para eksportir', 'Ada', 'Milik Sendiri', 'http://e-ska.kemendag.go.id', '80', 'e-SKA', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('56', 'Dinas Perpustakaan dan Kearsipan', 'INLISLite Versi 3 merupakan pengembangan lanjutan dari perangkat lunak (software) aplikasi otomasi perpustakaan INLISLite Versi 2 yang dibangun dan dikembangkan oleh perpustakaan Nasional RI sejak tahun 2011', 'Ada', 'Milik Sendiri', 'www.layanan.dispusip.bandung.go.id', '80', 'Aplikasi INLISLite V3', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('57', 'Dinas Perpustakaan dan Kearsipan', 'Sistem Informasi Perpustakaan Umum Taman Bacaan Masyarakat merupakan pengelolaan taman bacaan masyarakat berbasis website.', 'Ada', 'Milik Sendiri', 'dispusip.bandung.go.id/simacam', '80', 'SIMACAM', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('58', 'Dinas Perpustakaan dan Kearsipan', 'Perpustakaan dengan koleksi digital', 'Ada', 'Milik Sendiri', 'http://dispusip.bandung.go.id/penerapan-sistem-informasi-taman-bacaan-masyarakat-simacam-berbasis-web-e-library-di-kota-bandung/', '80', 'ELibrary', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('59', 'Dinas Perpustakaan dan Kearsipan', 'Sistem informasi kearsipan statis', 'Ada', 'Milik Sendiri', 'www.siks.dispusip.bandung.go.id', '80', 'Khazanah Kearsipan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('60', 'Dinas Perpustakaan dan Kearsipan', 'web informasi kedinasan', 'Ada', 'Milik Sendiri', 'www.dispusip.bandung.go.id', '80', 'Portal DInas Perpustakaan Dan Kearsipan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('61', 'Dinas Perpustakaan dan Kearsipan', 'Sistem informasi kearsipan dinamis', 'Ada', 'Milik Sendiri', 'www.sikd.dispusip.bandung.go.id', '80', 'SKD Kearsipan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('62', 'Dinas Perpustakaan dan Kearsipan', 'Data pokok Dispusip', '', '', 'dapopusip.dispusip.bandung.go.id', '80', 'Master Data', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('63', 'Dinas Perumahan dan Kawasan Permukiman, Pertanahan dan Pertamanan', 'Aplikasi e-office merupakan aplikasi perkantoran yang mengganti pproses administrasi berbasis manual', 'Ada', 'Milik Sendiri', 'www. dpkp3-bdg.com', '80', 'e-Office dinas perumahan dan kawasan permukiman, pertanahan dan pertamanan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('64', 'Dinas Sosial dan Penanggulangan Kemiskinan', 'Informasi data terpadu program penanganan fakir miskin dan PMKS', 'Ada', 'Milik Sendiri', 'https://aksen.bandung.go.id', '80', 'Aplikasi Kesejahteraan Sosial Excellent (Aksen)', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('65', 'Dinas Sosial dan Penanggulangan Kemiskinan', 'Informasi tentang kedinasan ', 'Ada', 'Milik Sendiri', 'https://dinsos.bandung.go.id', '80', 'Website Dinas Sosial Pemkot Bandung', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('66', 'Dinas Tenaga Kerja', '-', 'Ada', 'Milik Sendiri', 'disnaker.bandung.go.id', '80', 'Web BIMMA', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('67', 'Direktur PD. Bank Perkreditan Rakyat Kota Bandung', 'Pelaporan Data Nasabah Kredit yang terdaftar di PD. BPR Kota Bandung berikut Saldo dan Kolektibilitas Kreditnya', 'Tidak Ada', 'Milik Sendiri', 'Client', '80', 'Sistem Informasi Debitur (BI Checking)', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('68', 'Direktur PD. Kebersihan Kota Bandung', 'Informasi PD Kebersihan', 'Ada', 'Milik Sendiri', 'pdkebersihan.bandung.go.id', '80', 'Website PD Kebersihan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('69', 'Kecamatan Arcamanik', 'disposisi/surat perintah online', 'Ada', 'Milik Sendiri', 'http://deposisi.arcamanik.bandung.go.id/', '80', 'modis mobile disposisi', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('70', 'Kecamatan Astana Anyar', 'Memuat data dan informasi seputar Kecamatan Astanaanyar', 'Ada', 'Milik Sendiri', 'astanaanyar.com', '80', 'Website Kecamatan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('71', 'Kecamatan Bandung Wetan', 'Untuk Surat Meyurat dan Disposisi Surat', 'Tidak Ada', 'Milik Sendiri', 'www.bandungwetan.com/register', '80', 'Papperless Bandung Wetan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('72', 'Kecamatan Batununggal', '-', 'Ada', 'Milik Sendiri', 'batununggal.bandung.go.id', '80', 'Website Kecamatan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('73', 'Kecamatan Cibeunying Kidul', '-', 'Ada', 'Milik Sendiri', 'cibeunyingkidul.bandung.go.id', '80', 'Website Kecamatan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('74', 'Kecamatan Cinambo', '-', 'Ada', 'Milik Sendiri', 'cinambo.bandung.go.id', '80', 'Website Kecamtan Cinambo', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('75', 'Kecamatan Gedebage', 'Berisi informasi profil, dokumentasi, dan layanan kecamatan gedebage bseserta dengan kelurahan-kelurahan yang ada didalamnya.', 'Tidak Ada', 'Milik Sendiri', 'gedebage.bandung.go.id', '80', 'Website Kecamatan Gedebage', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('76', 'Kecamatan Mandalajati', '-', 'Ada', 'Milik Sendiri', 'mandalajati.bandung.go.id', '80', 'Website Kecamatan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('77', 'Kecamatan Panyileukan', 'website informasi Kecamatan Panyileukan', 'Ada', 'Milik Sendiri', 'panyileukan.bandung.go.id', '80', 'panyileukan online', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('78', 'Kecamatan Rancasari', '-', 'Ada', 'Milik Sendiri', 'rancasari.bandung.go.id', '80', 'Website Kecamatan Rancasari', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('79', 'Kecamatan Regol', '-', 'Ada', 'Milik Sendiri', 'regol.bandung.go.id', '80', 'Website Kecamatan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('80', 'Kecamatan Sukajadi', 'profil, prosedur dan kegiatan mengenai Kecamatan Sukajadi', 'Ada', 'Milik Sendiri', 'kecamatansukajadi.com', '80', 'Website Kecamatan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('81', 'Kecamatan Sukajadi', '-', 'Ada', 'Milik Sendiri', 'kecamatansukajadi.com/appkecamatan', '80', 'SIP Administrasi', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('82', 'Kecamatan Sukasari', '-', 'Ada', 'Milik Sendiri', 'sukasari.bandung.go.id', '80', 'Website Kecamatan', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('83', 'Kecamatan Sumur Bandung', '-', 'Ada', 'Milik Sendiri', 'sumurbandung.bandung.go.id', '80', 'Kecamatan Sumur Bandung', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);
INSERT INTO `monitor_servers` VALUES ('84', 'Satuan Polisi Pamong Praja', 'Website Satpol PP Kota Bandung', 'Ada', 'Milik Sendiri', 'www.satpolpp.bandung.go.id', '80', 'Website SatpolPP', 'website', null, 'on', null, null, null, null, 'yes', 'no', 'no', 'no', '1', '0', null, null, null, null);

-- ----------------------------
-- Table structure for monitor_servers_history
-- ----------------------------
DROP TABLE IF EXISTS `monitor_servers_history`;
CREATE TABLE `monitor_servers_history` (
  `servers_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) unsigned NOT NULL,
  `date` date NOT NULL,
  `latency_min` float(9,7) NOT NULL,
  `latency_avg` float(9,7) NOT NULL,
  `latency_max` float(9,7) NOT NULL,
  `checks_total` int(11) unsigned NOT NULL,
  `checks_failed` int(11) unsigned NOT NULL,
  PRIMARY KEY (`servers_history_id`),
  UNIQUE KEY `server_id_date` (`server_id`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_servers_history
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_servers_uptime
-- ----------------------------
DROP TABLE IF EXISTS `monitor_servers_uptime`;
CREATE TABLE `monitor_servers_uptime` (
  `servers_uptime_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `latency` float(9,7) DEFAULT NULL,
  PRIMARY KEY (`servers_uptime_id`),
  KEY `server_id` (`server_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_servers_uptime
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_users
-- ----------------------------
DROP TABLE IF EXISTS `monitor_users`;
CREATE TABLE `monitor_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) NOT NULL COMMENT 'user''s name, unique',
  `password` varchar(255) NOT NULL COMMENT 'user''s password in salted and hashed format',
  `password_reset_hash` char(40) DEFAULT NULL COMMENT 'user''s password reset code',
  `password_reset_timestamp` bigint(20) DEFAULT NULL COMMENT 'timestamp of the password reset request',
  `rememberme_token` varchar(64) DEFAULT NULL COMMENT 'user''s remember-me cookie token',
  `level` tinyint(2) unsigned NOT NULL DEFAULT '20',
  `name` varchar(255) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `pushover_key` varchar(255) NOT NULL,
  `pushover_device` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `unique_username` (`user_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_users
-- ----------------------------
INSERT INTO `monitor_users` VALUES ('1', 'admin', '$2y$10$1OcRAhSZWf7tSjwrOHGNp.umQl.3gOwswOxMz69isZrn9RkxjXRPe', null, null, null, '10', 'admin', '', '', '', 'adi8hamzah@gmail.com');

-- ----------------------------
-- Table structure for monitor_users_preferences
-- ----------------------------
DROP TABLE IF EXISTS `monitor_users_preferences`;
CREATE TABLE `monitor_users_preferences` (
  `user_id` int(11) unsigned NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_users_preferences
-- ----------------------------
INSERT INTO `monitor_users_preferences` VALUES ('1', 'status_layout', '0');

-- ----------------------------
-- Table structure for monitor_users_servers
-- ----------------------------
DROP TABLE IF EXISTS `monitor_users_servers`;
CREATE TABLE `monitor_users_servers` (
  `user_id` int(11) unsigned NOT NULL,
  `server_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`server_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of monitor_users_servers
-- ----------------------------
